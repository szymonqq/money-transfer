Technology stack:
* Java 8
* Lombok
* JDBC
* H2
* Spark
* Jackson
* Tests: Junit5, AssertJ, Mockito, RestAssured, Gson 

TODO: 
* integration tests (but tested using Postman)
