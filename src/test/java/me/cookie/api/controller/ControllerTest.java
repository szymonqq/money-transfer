package me.cookie.api.controller;


import io.restassured.RestAssured;
import io.restassured.mapper.ObjectMapperType;
import me.cookie.api.dto.*;
import me.cookie.service.AccountServiceFacade;
import me.cookie.service.InsufficientFundsException;
import me.cookie.service.SourceAccountNotFound;
import me.cookie.service.TargetAccountNotFound;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.Optional;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.mock;

@ExtendWith(MockitoExtension.class)
class ControllerTest {

    static Controller tested;

    @BeforeAll
    static void before() {
        RestAssured.port = 4567;
        RestAssured.baseURI = "http://localhost";
        RestAssured.basePath = "/accounts";
    }

    @Test
    void createAccountShouldReturn201WhenAccountCreated(@Mock AccountServiceFacade facade) {
        AccountCreateInput input = new AccountCreateInput(1L);
        AccountInfo result = new AccountInfo(1L, 1L);
        Mockito.when(facade.createAccount(input)).thenReturn(result);
        initializeController(facade);

        AccountInfo res = given()
                .body(input, ObjectMapperType.GSON)
                .when()
                .post()
                .then()
                .statusCode(201)
                .extract().as(AccountInfo.class);

        assertThat(res.getAccountId()).isEqualTo(1L);
        assertThat(res.getUserId()).isEqualTo(1L);
    }

    @Test
    void createAccountShouldReturn400WhenInputIsInvalid() {
        String input = "{ \"userId\": null }";
        initializeController();

        given()
                .body(input, ObjectMapperType.GSON)
                .when()
                .post()
                .then()
                .statusCode(400);
    }

    @Test
    void getAccountDetailsShouldReturn200WhenAccountFound(@Mock AccountServiceFacade facade) {
        AccountDetails details = new AccountDetails(1L, 2L, new BigDecimal(10.03));
        Mockito.when(facade.getAccountDetails(1L)).thenReturn(Optional.of(details));
        initializeController(facade);

        AccountDetails res = given()
                .pathParam("id", 1L)
                .when()
                .get("/{id}")
                .then()
                .statusCode(200)
                .extract().as(AccountDetails.class);

        assertThat(res.getAccountId()).isEqualTo(1L);
        assertThat(res.getUserId()).isEqualTo(2L);
        assertThat(res.getBalance()).usingComparator(BigDecimal::compareTo).isEqualTo(new BigDecimal(10.03));
    }

    @Test
    void getAccountDetailsShouldReturn404IfAccountDoesNotExist(@Mock AccountServiceFacade facade) {
        Mockito.when(facade.getAccountDetails(1L)).thenReturn(Optional.empty());
        initializeController(facade);

        given()
                .pathParam("id", 1L)
                .when()
                .get("/{id}")
                .then()
                .statusCode(404);
    }

    @Test
    void getAccountDetailsShouldReturn400ForInvalidId() {
        initializeController();

        given()
                .pathParam("id", "badId")
                .when()
                .get("/{id}")
                .then()
                .statusCode(400);
    }

    @Test
    void creditAccountShouldReturn204WhenSuccessful() {
        CreditAccountCommand input = new CreditAccountCommand(new BigDecimal(10));
        initializeController();

        given()
                .pathParam("id", 1L)
                .body(input)
                .when()
                .post("/{id}/credit")
                .then()
                .statusCode(204);
    }

    @Test
    void creditAccountShouldReturn400ForNegativeAmount() {
        CreditAccountCommand input = new CreditAccountCommand(new BigDecimal(-10));
        initializeController();

        given()
                .pathParam("id", 1L)
                .body(input)
                .when()
                .post("/{id}/credit")
                .then()
                .statusCode(400);
    }

    @Test
    void creditAccountShouldReturn404IfAccountNotFound(@Mock AccountServiceFacade facade) throws SourceAccountNotFound {
        CreditAccountCommand input = new CreditAccountCommand(new BigDecimal(10));
        Mockito.doThrow(SourceAccountNotFound.class).when(facade).creditAccount(anyLong(), any(CreditAccountCommand.class));
        initializeController(facade);

        given()
                .pathParam("id", 1L)
                .body(input)
                .when()
                .post("/{id}/credit")
                .then()
                .statusCode(404);
    }

    @Test
    void transferMoneyShouldReturn204WhenSuccessful() {
        TransferCommand input = new TransferCommand(2L, new BigDecimal(10));
        initializeController();

        given()
                .pathParam("id", 1L)
                .body(input)
                .when()
                .post("/{id}/transfer")
                .then()
                .statusCode(204);
    }

    @Test
    void transferMoneyShouldReturn404IfSourceAccountNotFound(@Mock AccountServiceFacade facade) throws TargetAccountNotFound, InsufficientFundsException, SourceAccountNotFound {
        TransferCommand input = new TransferCommand(2L, new BigDecimal(10));
        Mockito.doThrow(SourceAccountNotFound.class).when(facade).transferMoney(5L, input);
        initializeController(facade);

        given()
                .pathParam("id", 5L)
                .body(input)
                .when()
                .post("/{id}/transfer")
                .then()
                .statusCode(404);
    }

    @Test
    void transferMoneyShouldReturn400IfTargetAccountNotFound(@Mock AccountServiceFacade facade) throws TargetAccountNotFound, InsufficientFundsException, SourceAccountNotFound {
        TransferCommand input = new TransferCommand(2L, new BigDecimal(10));
        Mockito.doThrow(TargetAccountNotFound.class).when(facade).transferMoney(anyLong(), any(TransferCommand.class));
        initializeController(facade);

        given()
                .pathParam("id", 1L)
                .body(input)
                .when()
                .post("/{id}/transfer")
                .then()
                .statusCode(400);
    }

    @Test
    void transferMoneyShouldReturn409IfFundsAreNotSufficient(@Mock AccountServiceFacade facade) throws TargetAccountNotFound, InsufficientFundsException, SourceAccountNotFound {
        TransferCommand input = new TransferCommand(2L, new BigDecimal(10));
        Mockito.doThrow(InsufficientFundsException.class).when(facade).transferMoney(anyLong(), any(TransferCommand.class));
        initializeController(facade);

        given()
                .pathParam("id", 1L)
                .body(input)
                .when()
                .post("/{id}/transfer")
                .then()
                .statusCode(409);
    }

    @Test
    void transferMoneyShouldReturn400IfSourceIdMatchesTargetId() {
        TransferCommand input = new TransferCommand(1L, new BigDecimal(10));
        initializeController();

        given()
                .pathParam("id", 1L)
                .body(input)
                .when()
                .post("/{id}/transfer")
                .then()
                .statusCode(400);
    }

    private static void initializeController() {
        initializeController(mock(AccountServiceFacade.class));
    }

    private static void initializeController(AccountServiceFacade facade) {
        if (tested == null) {
            tested = new Controller(facade);
            tested.setupRoutes();
        } else {
            tested.setAccountFacade(facade);
        }
    }
}