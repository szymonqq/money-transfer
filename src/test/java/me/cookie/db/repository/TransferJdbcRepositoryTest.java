package me.cookie.db.repository;

import me.cookie.db.connection.ConnectionPool;
import me.cookie.db.connection.H2ConnectionPool;
import me.cookie.db.util.DbUtil;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.math.BigDecimal;
import java.sql.SQLException;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TransferJdbcRepositoryTest {

    TransferJdbcRepository tested;

    @BeforeAll
    void beforeAll() throws SQLException {
        ConnectionPool pool = new H2ConnectionPool();
        DbUtil.createTransferTable(pool.getConnection());
        tested = new TransferJdbcRepository(pool);
    }

    @Test
    void shouldCreditAccountAndGetBalance() {
        Long accId = 10L;
        BigDecimal amount = new BigDecimal(20);

        tested.creditAccount(accId, amount);
        BigDecimal balance = tested.getAccountBalance(accId);

        assertThat(balance).usingComparator(BigDecimal::compareTo).isEqualTo(amount);
    }

    @Test
    void shouldSaveTransactionAndGetBalance() throws SQLException {
        Long srcId = 1L;
        Long trgId = 2L;
        BigDecimal amount = new BigDecimal("10.234");

        tested.saveBidirectionalTransfer(srcId, trgId, amount);
        BigDecimal srcBalance = tested.getAccountBalance(srcId);
        BigDecimal trgBalance = tested.getAccountBalance(trgId);

        assertThat(srcBalance).usingComparator(BigDecimal::compareTo).isEqualTo(amount.negate());
        assertThat(trgBalance).usingComparator(BigDecimal::compareTo).isEqualTo(amount);
    }
}
