package me.cookie.db.repository;

import me.cookie.db.connection.ConnectionPool;
import me.cookie.db.connection.H2ConnectionPool;
import me.cookie.db.util.DbUtil;
import me.cookie.model.Account;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class AccountJdbcRepositoryTest {

    AccountJdbcRepository tested;

    @BeforeAll
    void beforeAll() throws SQLException {
        ConnectionPool pool = new H2ConnectionPool();
        DbUtil.createAccountTable(pool.getConnection());
        tested = new AccountJdbcRepository(pool);
    }

    @Test
    void createTwoAccountsAndFindAll() {
        Set<Long> userIds = new HashSet<>(Arrays.asList(1L, 2L));
        tested.create(account(1L));
        tested.create(account(2L));

        Collection<Account> all = tested.findAll();

        assertThat(all).hasSize(2);
        assertThat(all).allMatch(account -> account.getId() != null);
        assertThat(all).allMatch(account -> userIds.contains(account.getUserId()));
    }

    Account account(Long userId) {
        return Account.builder()
                .userId(userId)
                .build();
    }
}