package me.cookie.mapper;

import me.cookie.api.dto.AccountCreateInput;
import me.cookie.api.dto.AccountDetails;
import me.cookie.api.dto.AccountInfo;
import me.cookie.model.Account;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;


class AccountMapperTest {

    AccountMapper tested = new AccountMapper();

    @Test
    void fromAccountCreateInputToModel() {
        AccountCreateInput input = new AccountCreateInput(1L);
        Account result = tested.toModel(input);
        assertThat(result.getId()).isNull();
        assertThat(result.getUserId()).isEqualTo(1L);
    }

    @Test
    void fromModelToAccountInfo() {
        Account model = Account.builder().id(1L).userId(2L).build();
        AccountInfo result = tested.accountInfo(model);
        assertThat(result.getAccountId()).isEqualTo(1L);
        assertThat(result.getUserId()).isEqualTo(2L);
    }

    @Test
    void fromModelToAccountDetails() {
        Account model = Account.builder().id(1L).userId(2L).build();
        BigDecimal balance = new BigDecimal(20);
        AccountDetails result = tested.accountDetails(model, balance);
        assertThat(result.getAccountId()).isEqualTo(1L);
        assertThat(result.getUserId()).isEqualTo(2L);
        assertThat(result.getBalance()).usingComparator(BigDecimal::compareTo).isEqualTo(balance);
    }
}