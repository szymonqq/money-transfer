package me.cookie.service;

import me.cookie.api.dto.AccountDetails;
import me.cookie.api.dto.TransferCommand;
import me.cookie.mapper.AccountMapper;
import me.cookie.model.Account;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatExceptionOfType;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AccountServiceFacadeTest {

    @Mock
    AccountService accountService;
    @Mock
    TransferService transferService;
    @Mock
    AccountMapper mapper;

    AccountServiceFacade tested;

    @AfterEach
    void afterEach() {
        Mockito.reset(accountService);
    }

    @Test
    void shouldCallMapperWithCorrectArgs() {
        Long id = 1L;
        Account account = Account.builder().id(id).userId(2L).build();
        BigDecimal balance = new BigDecimal(10);
        when(accountService.get(id)).thenReturn(Optional.of(account));
        when(transferService.getBalance(id)).thenReturn(balance);

        tested = new AccountServiceFacade(accountService, transferService, mapper);
        tested.getAccountDetails(id);

        verify(mapper).accountDetails(account, balance);

    }

    @Test
    void shouldReturnEmptyOptionalIfAccountNotFound() {
        Long id = 1L;
        when(accountService.get(id)).thenReturn(Optional.empty());

        tested = new AccountServiceFacade(accountService, transferService, mapper);
        Optional<AccountDetails> result = tested.getAccountDetails(id);

        assertThat(result).isEmpty();
    }

    @Test
    void creditShouldThrowExceptionWhenSourceAccountDoesNotExist() {
        Long id = 1L;
        when(accountService.get(id)).thenReturn(Optional.empty());

        tested = new AccountServiceFacade(accountService, transferService, mapper);

        assertThatExceptionOfType(SourceAccountNotFound.class)
                .isThrownBy(() -> tested.transferMoney(id, null));
    }

    @Test
    void transferShouldThrowExceptionWhenSourceAccountDoesNotExist() {
        Long id = 1L;
        when(accountService.get(id)).thenReturn(Optional.empty());

        tested = new AccountServiceFacade(accountService, transferService, mapper);

        assertThatExceptionOfType(SourceAccountNotFound.class)
                .isThrownBy(() -> tested.transferMoney(id, null));
    }

    @Test
    void transferShouldThrowExceptionWhenTargetAccountDoesNotExist() {
        TransferCommand command = new TransferCommand();
        command.setTargetAccountId(1L);
        when(accountService.get(any())).thenReturn(Optional.of(Account.builder().build()), Optional.empty());

        tested = new AccountServiceFacade(accountService, transferService, mapper);

        assertThatExceptionOfType(TargetAccountNotFound.class)
                .isThrownBy(() -> tested.transferMoney(2L, command));
    }

    @Test
    void shouldCallTransferMoneyWhenBothAccountsExist()
            throws SourceAccountNotFound, TargetAccountNotFound, InsufficientFundsException {
        TransferCommand command = new TransferCommand();
        command.setTargetAccountId(2L);
        command.setAmount(BigDecimal.ONE);
        when(accountService.get(any())).thenReturn(Optional.of(Account.builder().build()));

        tested = new AccountServiceFacade(accountService, transferService, mapper);
        tested.transferMoney(1L, command);

        verify(transferService).transferMoney(any(Account.class), any(Account.class), any(BigDecimal.class));
    }
}