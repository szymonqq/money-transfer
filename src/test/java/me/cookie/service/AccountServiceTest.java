package me.cookie.service;

import me.cookie.db.repository.AccountRepository;
import me.cookie.model.Account;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AccountServiceTest {

    @Mock
    AccountRepository repository;
    AccountService tested;

    @Test
    void shouldSaveAccount() throws SQLException {
        Account nullObject = account(null);
        Account firstAcc = account(1L);
        when(repository.create(nullObject)).thenReturn(firstAcc);

        tested = new AccountService(repository);
        Account account = tested.create(nullObject);

        assertThat(account).isEqualTo(firstAcc);
    }

    @Test
    void shouldFindAllAccountsLoadedFromDb() throws SQLException {
        Account first = account(1L);
        Account second = account(2L);
        when(repository.findAll()).thenReturn(Arrays.asList(first, second));

        tested = new AccountService(repository);
        List<Account> found = tested.getAll();

        assertThat(found).hasSize(2);
        assertThat(found.get(0)).isEqualTo(first);
        assertThat(found.get(1)).isEqualTo(second);
    }

    Account account(Long accId) {
        return Account.builder()
                .id(accId)
                .build();
    }
}