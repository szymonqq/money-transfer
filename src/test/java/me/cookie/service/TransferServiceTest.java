package me.cookie.service;

import me.cookie.db.repository.TransferRepository;
import me.cookie.model.Account;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;

import static org.assertj.core.api.AssertionsForClassTypes.assertThatExceptionOfType;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class TransferServiceTest {

    @Mock
    TransferRepository repository;
    TransferService tested;

    final Long srcAccId = 1L;
    final Long trgAccId = 2L;
    final Account srcAcc = account(srcAccId);
    final Account trgAcc = account(trgAccId);
    final BigDecimal balance = new BigDecimal(20);

    @BeforeEach
    void before() {
        tested = new TransferService(repository);
    }

    @Test
    void shouldThrowInsufficientFundsException() {
        when(repository.getAccountBalance(srcAccId)).thenReturn(balance);

        assertThatExceptionOfType(InsufficientFundsException.class)
                .isThrownBy(() -> tested.transferMoney(srcAcc, trgAcc, new BigDecimal("20.000001")));
    }

    @Test
    void shouldCallSaveTransactionWhenFundsAvailable() throws InsufficientFundsException {
        when(repository.getAccountBalance(srcAccId)).thenReturn(balance);

        tested.transferMoney(srcAcc, trgAcc, balance);

        verify(repository).getAccountBalance(srcAccId);
        verify(repository).saveBidirectionalTransfer(srcAccId, trgAccId, balance);
    }

    @Test
    void shouldDoNothingWhenSourceIsTarget() throws InsufficientFundsException {
        tested.transferMoney(srcAcc, srcAcc, balance);

        verify(repository, never()).getAccountBalance(any());
        verify(repository, never()).saveBidirectionalTransfer(any(), any(), any());
    }

    Account account(Long accId) {
        return Account.builder()
                .id(accId)
                .build();
    }
}