import me.cookie.db.repository.TransferRepository;
import me.cookie.model.Account;
import me.cookie.service.TransferService;

import java.math.BigDecimal;
import java.util.Random;

public class TransferServiceDeadlockTest {

    static final int NUMBER_OF_THREADS = 5;
    static final int NUMBER_OF_ACCOUNTS = 3;
    static final int NUMBER_OF_ITERATIONS = 1000000;
    static final Random random = new Random();
    static final BigDecimal BALANCE = new BigDecimal(1);

    public static void main(String[] args) throws InterruptedException {
        TransferService service = new TransferService(
                new TransferRepository() {
                    @Override
                    public void creditAccount(Long accountId, BigDecimal amount) {
                    }

                    @Override
                    public void saveBidirectionalTransfer(Long sourceAccId, Long targetAccId, BigDecimal amount) {
                    }

                    @Override
                    public BigDecimal getAccountBalance(Long accountId) {
                        return BALANCE;
                    }
                });


        Account[] accounts = new Account[NUMBER_OF_ACCOUNTS];
        for (int i = 0; i < NUMBER_OF_ACCOUNTS; i++) {
            accounts[i] = Account.builder()
                    .id((long) i)
                    .build();
        }

        Thread[] threads = new Thread[NUMBER_OF_THREADS];
        for (int i = 0; i < NUMBER_OF_THREADS; i++) {
            Thread thread = new Thread(() -> {
                for (int j = 0; j < NUMBER_OF_ITERATIONS; j++) {
                    int src = random.nextInt(NUMBER_OF_ACCOUNTS);
                    int trg = random.nextInt(NUMBER_OF_ACCOUNTS);
                    try {
                        service.transferMoney(accounts[src], accounts[trg], BALANCE);
                    } catch (Exception ignored) {
                    }
                }
            });
            threads[i] = thread;
            thread.start();
        }
        for (int i = 0; i < NUMBER_OF_THREADS; i++) {
            threads[i].join();
        }
    }
}
