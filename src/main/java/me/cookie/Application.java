package me.cookie;

import lombok.extern.slf4j.Slf4j;
import me.cookie.api.controller.Controller;
import me.cookie.db.connection.ConnectionPool;
import me.cookie.db.connection.H2ConnectionPool;
import me.cookie.db.repository.AccountJdbcRepository;
import me.cookie.db.repository.AccountRepository;
import me.cookie.db.repository.TransferJdbcRepository;
import me.cookie.db.repository.TransferRepository;
import me.cookie.db.util.DbUtil;
import me.cookie.mapper.AccountMapper;
import me.cookie.service.AccountService;
import me.cookie.service.AccountServiceFacade;
import me.cookie.service.TransferService;

import java.sql.Connection;
import java.sql.SQLException;

import static me.cookie.db.util.DbUtil.close;

@Slf4j
public class Application {

    public static void main(String[] args) {
        ConnectionPool connectionPool = new H2ConnectionPool();
        Connection conn = connectionPool.getConnection();
        try {
            DbUtil.setup(conn);
        } catch (SQLException e) {
            close(conn);
            log.error("Error on db setup", e);
            return;
        }

        AccountRepository accountRepository = new AccountJdbcRepository(connectionPool);
        TransferRepository transferRepository = new TransferJdbcRepository(connectionPool);
        AccountService accountService = new AccountService(accountRepository);
        TransferService transferService = new TransferService(transferRepository);
        AccountServiceFacade facade = new AccountServiceFacade(accountService, transferService, new AccountMapper());
        Controller controller = new Controller(facade);

        controller.setupRoutes();
    }

}
