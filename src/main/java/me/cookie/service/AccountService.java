package me.cookie.service;

import me.cookie.db.repository.AccountRepository;
import me.cookie.model.Account;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ConcurrentMap;
import java.util.function.Function;
import java.util.stream.Collectors;

public class AccountService {

    private final AccountRepository repository;
    private final ConcurrentMap<Long, Account> accounts;

    public AccountService(AccountRepository repository) {
        this.repository = repository;
        this.accounts = loadAccounts();
    }

    private ConcurrentMap loadAccounts() {
        return repository.findAll().stream()
                .collect(Collectors.toConcurrentMap(Account::getId, Function.identity()));
    }

    public Account create(Account account) {
        Account created = repository.create(account);
        accounts.put(created.getId(), created);
        return created;
    }

    public List<Account> getAll() {
        return new ArrayList<>(accounts.values());
    }

    public Optional<Account> get(Long id) {
        return Optional.ofNullable(accounts.get(id));
    }
}
