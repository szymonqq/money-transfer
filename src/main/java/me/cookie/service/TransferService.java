package me.cookie.service;

import lombok.RequiredArgsConstructor;
import me.cookie.db.repository.TransferRepository;
import me.cookie.model.Account;

import java.math.BigDecimal;

@RequiredArgsConstructor
public class TransferService {

    private final TransferRepository transferRepository;

    public void transferMoney(Account sourceAcc, Account targetAcc, BigDecimal amount)
            throws InsufficientFundsException {
        if (sourceAcc.getId() < targetAcc.getId()) {
            synchronized (sourceAcc) {
                synchronized (targetAcc) {
                    makeTransfer(sourceAcc, targetAcc, amount);
                }
            }
        } else if (sourceAcc.getId() > targetAcc.getId()) {
            synchronized (targetAcc) {
                synchronized (sourceAcc) {
                    makeTransfer(sourceAcc, targetAcc, amount);
                }
            }
        }
    }

    public void creditAccount(Account account, BigDecimal amount) {
        synchronized (account) {
            transferRepository.creditAccount(account.getId(), amount);
        }
    }

    private void makeTransfer(Account sourceAcc, Account targetAcc, BigDecimal amount)
            throws InsufficientFundsException {
        BigDecimal balance = transferRepository.getAccountBalance(sourceAcc.getId());
        if (balance.compareTo(amount) >= 0) {
            transferRepository.saveBidirectionalTransfer(sourceAcc.getId(), targetAcc.getId(), amount);
        } else {
            throw new InsufficientFundsException();
        }
    }

    public BigDecimal getBalance(Long accId) {
        return transferRepository.getAccountBalance(accId);
    }
}
