package me.cookie.service;

import lombok.extern.slf4j.Slf4j;
import me.cookie.api.dto.*;
import me.cookie.mapper.AccountMapper;
import me.cookie.model.Account;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Slf4j
public class AccountServiceFacade {

    private final AccountService accountService;
    private final TransferService transferService;
    private final AccountMapper mapper;

    public AccountServiceFacade(AccountService accountService, TransferService transferService, AccountMapper mapper) {
        this.accountService = accountService;
        this.transferService = transferService;
        this.mapper = mapper;
    }

    public AccountInfo createAccount(AccountCreateInput dto) {
        Account created = accountService.create(mapper.toModel(dto));
        return mapper.accountInfo(created);
    }

    public List<AccountInfo> getAllAccounts() {
        List<Account> all = accountService.getAll();
        return mapper.accountInfo(all);
    }

    public Optional<AccountDetails> getAccountDetails(Long id) {
        return accountService.get(id)
                .map(this::buildAccountDetails);
    }

    public void creditAccount(Long id, CreditAccountCommand command) throws SourceAccountNotFound {
        Optional<Account> account = accountService.get(id);
        if (account.isPresent()) {
            transferService.creditAccount(account.get(), command.getAmount());
        } else {
            throw new SourceAccountNotFound();
        }
    }

    public void transferMoney(Long srcAccId, TransferCommand transfer)
            throws SourceAccountNotFound, TargetAccountNotFound, InsufficientFundsException {
        Optional<Account> srcAcc = accountService.get(srcAccId);
        if (srcAcc.isPresent()) {
            Optional<Account> trgAcc = accountService.get(transfer.getTargetAccountId());
            if (trgAcc.isPresent()) {
                transferService.transferMoney(srcAcc.get(), trgAcc.get(), transfer.getAmount());
            } else {
                throw new TargetAccountNotFound();
            }
        } else {
            throw new SourceAccountNotFound();
        }
    }

    private AccountDetails buildAccountDetails(Account account) {
        BigDecimal balance = transferService.getBalance(account.getId());
        return mapper.accountDetails(account, balance);
    }
}
