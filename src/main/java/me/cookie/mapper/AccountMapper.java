package me.cookie.mapper;

import me.cookie.api.dto.AccountCreateInput;
import me.cookie.api.dto.AccountDetails;
import me.cookie.api.dto.AccountInfo;
import me.cookie.model.Account;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

public class AccountMapper {

    public Account toModel(AccountCreateInput dto) {
        return Account.builder()
                .userId(dto.getUserId())
                .build();
    }

    public AccountInfo accountInfo(Account model) {
        return new AccountInfo(model.getId(), model.getUserId());
    }

    public List<AccountInfo> accountInfo(List<Account> model) {
        return model.stream()
                .map(this::accountInfo)
                .collect(Collectors.toList());
    }

    public AccountDetails accountDetails(Account model, BigDecimal balance) {
        return new AccountDetails(model.getId(), model.getUserId(), balance);
    }
}
