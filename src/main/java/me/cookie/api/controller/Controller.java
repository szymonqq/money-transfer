package me.cookie.api.controller;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import lombok.extern.slf4j.Slf4j;
import me.cookie.api.dto.*;
import me.cookie.service.AccountServiceFacade;
import me.cookie.service.InsufficientFundsException;
import me.cookie.service.SourceAccountNotFound;
import me.cookie.service.TargetAccountNotFound;

import java.math.BigDecimal;
import java.util.Optional;

import static spark.Spark.*;

@Slf4j
public class Controller {

    private static final ErrorMessage INVALID_TRG_ID_MSG = new ErrorMessage("Invalid target account id");
    private static final ErrorMessage INSUFFICIENT_FUNDS_MSG = new ErrorMessage("Insufficient funds");
    private static final ObjectMapper reader = new ObjectMapper();
    private static final ObjectWriter writer;
    private AccountServiceFacade facade;

    static {
        ObjectMapper writerMapper = new ObjectMapper();
        writerMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        writer = writerMapper.writer();
    }

    public Controller(AccountServiceFacade facade) {
        this.facade = facade;
    }

    synchronized void setAccountFacade(AccountServiceFacade facade) {
        this.facade = facade;
    }

    public void setupRoutes() {

        before((req, res) -> {
            log.info(req.pathInfo() + " " + req.body());
            res.type("application/json");
        });

        path("/accounts", () -> {

            get("",
                    ((req, res) -> facade.getAllAccounts()),
                    writer::writeValueAsString);

            post("",
                    ((req, res) -> {
                        AccountCreateInput dto = reader.readValue(req.body(), AccountCreateInput.class);
                        AccountInfo created = facade.createAccount(dto);
                        res.status(201);
                        return created;
                    }),
                    writer::writeValueAsString);

            get("/:id",
                    ((req, res) ->
                    {
                        Optional<AccountDetails> acc = facade.getAccountDetails(Long.valueOf(req.params(":id")));
                        if (acc.isPresent()) {
                            return acc.get();
                        }
                        res.status(404);
                        return res;
                    })
                    , writer::writeValueAsString);

            post("/:id/credit",
                    ((req, res) ->
                    {
                        Long accId = Long.valueOf(req.params(":id"));
                        CreditAccountCommand command = reader.readValue(req.body(), CreditAccountCommand.class);
                        if (command.getAmount().compareTo(BigDecimal.ZERO) <= 0) {
                            res.status(400);
                        } else {
                            try {
                                facade.creditAccount(accId, command);
                                res.status(204);
                            } catch (SourceAccountNotFound e) {
                                res.status(404);
                            }
                        }
                        return res;
                    })
                    , writer::writeValueAsString);

            post("/:id/transfer",
                    ((req, res) -> {
                        Long sourceAccId = Long.valueOf(req.params(":id"));
                        TransferCommand transfer = reader.readValue(req.body(), TransferCommand.class);
                        if (!sourceAccId.equals(transfer.getTargetAccountId())) {
                            try {
                                facade.transferMoney(sourceAccId, transfer);
                                res.status(204);
                                return res;
                            } catch (SourceAccountNotFound e) {
                                res.status(404);
                                return res;
                            } catch (TargetAccountNotFound e) {
                                res.status(400);
                                return INVALID_TRG_ID_MSG;
                            } catch (InsufficientFundsException e) {
                                res.status(409);
                                return INSUFFICIENT_FUNDS_MSG;
                            }
                        } else {
                            res.status(400);
                            return INVALID_TRG_ID_MSG;
                        }
                    })
                    , writer::writeValueAsString);
        });

        exception(NumberFormatException.class, (ex, req, res) -> {
            res.status(400);
            res.body(ex.getMessage());
        });

        exception(JsonMappingException.class, (ex, req, res) -> {
            res.status(400);
            res.body(ex.getPathReference());
        });
    }

}
