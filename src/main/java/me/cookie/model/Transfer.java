package me.cookie.model;

import lombok.Builder;
import lombok.Value;

import java.math.BigDecimal;
import java.time.Instant;

@Value
@Builder
public class Transfer {
    Instant timestamp;
    Long sourceAccId;
    Long targetAccId;
    BigDecimal amount;
}
