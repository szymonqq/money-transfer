package me.cookie.model;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class Account {
    Long id;
    Long userId;
}
