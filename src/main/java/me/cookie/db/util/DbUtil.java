package me.cookie.db.util;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class DbUtil {

    private static final String CREATE_ACCOUNT_TABLE = "CREATE TABLE IF NOT EXISTS account ( " +
            "id BIGINT NOT NULL AUTO_INCREMENT, " +
            "user_id BIGINT NOT NULL, " +
            "PRIMARY KEY (id))";

    private static final String CREATE_TRANSFER_TABLE = "CREATE TABLE IF NOT EXISTS transfer ( " +
            "transfer_timestamp BIGINT NOT NULL, " +
            "source_id BIGINT NOT NULL, " +
            "target_id BIGINT, " +
            "amount DECIMAL NOT NULL, " +
            "PRIMARY KEY (transfer_timestamp, source_id))";

    private static final String ADD_FOREIGN_KEY_SRC = "ALTER TABLE transfer " +
            "ADD FOREIGN KEY (source_id) REFERENCES account (id)";

    private static final String ADD_FOREIGN_KEY_TRG = "ALTER TABLE transfer " +
            "ADD FOREIGN KEY (target_id) REFERENCES account (id)";

    public static void setup(Connection conn) throws SQLException {
        createAccountTable(conn);
        createTransferTable(conn);
        addConstraints(conn);
        conn.commit();
    }

    public static void close(AutoCloseable closeable) {
        try {
            if (closeable != null) {
                closeable.close();
            }
        } catch (Exception ignore) {
        }
    }

    public static void createAccountTable(Connection conn) throws SQLException {
        execute(conn, CREATE_ACCOUNT_TABLE);
    }

    public static void createTransferTable(Connection conn) throws SQLException {
        execute(conn, CREATE_TRANSFER_TABLE);
    }

    private static void addConstraints(Connection conn) throws SQLException {
        execute(conn, ADD_FOREIGN_KEY_SRC);
        execute(conn, ADD_FOREIGN_KEY_TRG);
    }

    private static void execute(Connection conn, String sql) throws SQLException {
        Statement statement = conn.createStatement();
        statement.executeUpdate(sql);
        statement.close();
    }
}
