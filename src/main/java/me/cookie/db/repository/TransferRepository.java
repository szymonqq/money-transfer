package me.cookie.db.repository;

import java.math.BigDecimal;

public interface TransferRepository {

    void creditAccount(Long accountId, BigDecimal amount);

    void saveBidirectionalTransfer(Long sourceAccId, Long targetAccId, BigDecimal amount);

    BigDecimal getAccountBalance(Long accountId);
}
