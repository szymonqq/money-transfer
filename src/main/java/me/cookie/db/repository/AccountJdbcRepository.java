package me.cookie.db.repository;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import me.cookie.db.connection.ConnectionPool;
import me.cookie.db.exception.DatabaseException;
import me.cookie.model.Account;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static me.cookie.db.util.DbUtil.close;

@Slf4j
@RequiredArgsConstructor
public class AccountJdbcRepository implements AccountRepository {

    private static final String SELECT_ALL = "SELECT * FROM account";
    private static final String INSERT = "INSERT INTO account (user_id) VALUES (?)";
    private final ConnectionPool connPool;

    @Override
    public Collection<Account> findAll() {
        Connection conn = connPool.getConnection();
        PreparedStatement findAll = null;
        ResultSet rs = null;
        try {
            findAll = conn.prepareStatement(SELECT_ALL);
            ResultSet resultSet = findAll.executeQuery();
            List<Account> all = new ArrayList<>();
            while (resultSet.next()) {
                all.add(Account.builder()
                        .id(resultSet.getLong("id"))
                        .userId(resultSet.getLong("user_id"))
                        .build());
            }
            return all;
        } catch (SQLException e) {
            log.error("Error while getting all accounts", e);
            throw new DatabaseException(e);
        } finally {
            close(rs);
            close(findAll);
            close(conn);
        }
    }

    @Override
    public Account create(Account account) {
        Connection conn = connPool.getConnection();
        PreparedStatement create = null;
        ResultSet rs = null;
        try {
            log.debug("Creating account {}", account);
            create = conn.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
            create.setLong(1, account.getUserId());
            create.executeUpdate();
            rs = create.getGeneratedKeys();
            if (rs.first()) {
                return Account.builder()
                        .id(rs.getLong("id"))
                        .userId(account.getUserId())
                        .build();
            } else {
                log.error("Severe error, couldn't generate account id!");
                throw new DatabaseException();
            }
        } catch (SQLException e) {
            log.error("Error while creating account {}", account, e);
            throw new DatabaseException(e);
        } finally {
            close(rs);
            close(create);
            close(conn);
        }
    }
}
