package me.cookie.db.repository;

import me.cookie.model.Account;

import java.util.Collection;

public interface AccountRepository {

    Collection<Account> findAll();

    Account create(Account account);
}
