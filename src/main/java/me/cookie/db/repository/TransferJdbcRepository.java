package me.cookie.db.repository;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import me.cookie.db.connection.ConnectionPool;
import me.cookie.db.exception.DatabaseException;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;

import static me.cookie.db.util.DbUtil.close;

@Slf4j
@RequiredArgsConstructor
public class TransferJdbcRepository implements TransferRepository {

    private static final String INSERT = "INSERT INTO transfer " +
            "VALUES (?, ?, ?, ?)";
    private static final String GET_BALANCE = "SELECT SUM(amount) from transfer " +
            "WHERE source_id = ?";
    private final ConnectionPool connPool;

    @Override
    public void creditAccount(Long accountId, BigDecimal amount) {
        Connection conn = connPool.getConnection();
        PreparedStatement creditAccount = null;
        try {
            log.debug("Credit account {}, amount={}", accountId, amount);
            long timestamp = Instant.now().toEpochMilli();
            creditAccount = prepareTransfer(conn, timestamp, accountId, accountId, amount);
            creditAccount.executeUpdate();
            conn.commit();
        } catch (SQLException e) {
            log.error("Error while crediting account {}", accountId, e);
            throw new DatabaseException(e);
        } finally {
            close(creditAccount);
            close(conn);
        }
    }

    @Override
    public void saveBidirectionalTransfer(Long sourceAccId, Long targetAccId, BigDecimal amount) {
        Connection conn = connPool.getConnection();
        PreparedStatement chargeSource = null;
        PreparedStatement creditTarget = null;
        try {
            log.debug("Transfer from account {} to account {}, amount={}", sourceAccId, targetAccId, amount);
            conn.setAutoCommit(false);
            long timestamp = Instant.now().toEpochMilli();
            chargeSource = prepareTransfer(conn, timestamp, sourceAccId, targetAccId, amount.negate());
            creditTarget = prepareTransfer(conn, timestamp, targetAccId, sourceAccId, amount);
            chargeSource.executeUpdate();
            creditTarget.executeUpdate();
            conn.commit();
            conn.setAutoCommit(true);
        } catch (SQLException e) {
            try {
                conn.rollback();
            } catch (SQLException ignore) {
                log.error("Severe error, couldn't rollback transaction!");
            }
            log.error("Error during transfer from account {} to account {}", sourceAccId, targetAccId, e);
            throw new DatabaseException(e);
        } finally {
            close(chargeSource);
            close(creditTarget);
            close(conn);
        }
    }

    @Override
    public BigDecimal getAccountBalance(Long accountId) {
        Connection conn = connPool.getConnection();
        PreparedStatement getBalance = null;
        ResultSet rs = null;
        try {
            getBalance = conn.prepareStatement(GET_BALANCE);
            getBalance.setLong(1, accountId);
            rs = getBalance.executeQuery();
            rs.first();
            BigDecimal balance = rs.getBigDecimal(1);
            return balance != null ? balance : BigDecimal.ZERO;
        } catch (SQLException e) {
            log.error("Error while getting balance for account {}", accountId, e);
            throw new DatabaseException(e);
        } finally {
            close(rs);
            close(getBalance);
            close(conn);
        }
    }

    private PreparedStatement prepareTransfer(Connection conn, Long ts, Long src, Long trg, BigDecimal amount)
            throws SQLException {
        PreparedStatement ps = conn.prepareStatement(INSERT);
        ps.setLong(1, ts);
        ps.setLong(2, src);
        ps.setLong(3, trg);
        ps.setBigDecimal(4, amount);
        return ps;
    }
}
