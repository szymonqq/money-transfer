package me.cookie.db.connection;

import java.sql.Connection;

public interface ConnectionPool {

    Connection getConnection();
}
