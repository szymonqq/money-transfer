package me.cookie.db.connection;

import lombok.extern.slf4j.Slf4j;
import me.cookie.db.exception.DatabaseException;
import org.h2.jdbcx.JdbcConnectionPool;

import java.sql.Connection;
import java.sql.SQLException;

@Slf4j
public class H2ConnectionPool implements ConnectionPool {

    private final JdbcConnectionPool pool;

    public H2ConnectionPool() {
        pool = JdbcConnectionPool.create("jdbc:h2:mem:test", "sa", "");
    }

    @Override
    public Connection getConnection() {
        try {
            return pool.getConnection();
        } catch (SQLException e) {
            log.error("Cannot get connection from H2 JdbcConnectionPoll!", e);
            throw new DatabaseException(e);
        }
    }
}
